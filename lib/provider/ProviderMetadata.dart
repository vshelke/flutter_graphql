import 'package:flutter/material.dart';

class ModelAppData {
  String token;
  String userId;
}

class ProviderMetadata with ChangeNotifier {
  String usertoken = '';

  String get getUserToken {
    return usertoken;
  }

  void setUserToken(String token) {
    usertoken = token;
    notifyListeners();
  }
}
