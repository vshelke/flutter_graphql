import 'package:flutter/cupertino.dart';

class DrawerItem {
  String id;
  String title;
  IconData icon;
  String permissions;
  DrawerItem(this.id, this.title, this.icon, this.permissions);
}
