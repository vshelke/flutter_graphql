class ModelAddress {
  String id;
  String address;
  String district;
  String taluka;
  String state;
  double latitude;
  double longitude;
  ModelAddress({
    this.id,
    this.address,
    this.district,
    this.taluka,
    this.state,
    this.latitude,
    this.longitude,
  });
}
