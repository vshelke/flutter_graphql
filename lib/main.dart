import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:fleet_flutter/modules/authentication/ScreenLogin.dart';
import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'helper/IConstant.dart';
import 'helper/MutationHelper.dart';

import 'dart:async';

bool isLogin = false;
String token;

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();

  // Pass all uncaught errors from the framework to Crashlytics.
  SharedPreferences prefs = await SharedPreferences.getInstance();

  // Pass all uncaught errors to Crashlytics.

  if (prefs.getBool(IConstanct.session) != null) {
    isLogin = prefs.getBool(IConstanct.session);
    token = prefs.getString(IConstanct.token);
    print(token);
  }

  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  // This widget is the root of your application.
  static const routeName = '/MyApp';
  ValueNotifier<GraphQLClient> client;
  HttpLink httpLink;

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey();

  String _message = '';

  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();

  _register() {
    _firebaseMessaging
        .getToken()
        .then((token) => print("Firebase ToKen " + token));
  }

  void getMessage() {
    _firebaseMessaging.configure(
        onMessage: (Map<String, dynamic> message) async {
      print('on message $message');
      // setState(() => _message = message["notification"]["title"]);
    }, onResume: (Map<String, dynamic> message) async {
      print('on resume $message');
      // setState(() => _message = message["notification"]["title"]);
    }, onLaunch: (Map<String, dynamic> message) async {
      print('on launch $message');
      //setState(() => _message = message["notification"]["title"]);
    });
  }

  sData() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    isLogin = prefs.getBool(IConstanct.session);
    token = prefs.getString(IConstanct.token);

    print(isLogin);
  }

  @override
  void initState() {
    super.initState();
    sData();
    getMessage();

    _register();
  }

  void _showError(dynamic exception) {
    _scaffoldKey.currentState
        .showSnackBar(SnackBar(content: Text(exception.toString())));
  }

  @override
  Widget build(BuildContext context) {
    widget.httpLink = MutationHelper.link(token);
    widget.client = ValueNotifier(
      GraphQLClient(
        cache: InMemoryCache(),
        link: widget.httpLink,
      ),
    );
    return GraphQLProvider(
      client: widget.client,
      child: MaterialApp(
        title: 'Fleet',
        theme: ThemeData(primarySwatch: Colors.lightGreen),
        initialRoute: '/',
        routes: {
          '/': (ctx) => isLogin
              ? Text("You are Logined")
              : ScreenLogin(
                  userToken: (userToekn) {
                    setState(() {
                      token = userToekn;
                      isLogin = true;
                      widget.httpLink = MutationHelper.link(token);
                      widget.client = ValueNotifier(
                        GraphQLClient(
                          cache: InMemoryCache(),
                          link: widget.httpLink,
                        ),
                      );
                    });
                  },
                ),
          MyApp.routeName: (ctx) => MyApp(),
        },
      ),
    );
  }
}
