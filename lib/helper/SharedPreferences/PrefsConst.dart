class PrefsConst {
  static const String TUTORIAL_STRING = 'TUTORIAL_STRING';

  // Default preferences
  static String session = 'login_session';
  static String token = 'userToken';
  static String demartmentId = 'demartmentId';
}
