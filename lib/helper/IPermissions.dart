class IPermission {
  static String session = 'vt_core.view_purchaseorderitem';
  static String empty = ' ';

  static String token = 'userToken';
  static String ruppes = '₹ ';

  static String permission_benchmark_recommendation =
      "vt_core.vt_app_benchmark_recommendation";
  static String permission_benchmarking_officer =
      "vt_core.vt_app_benchmarking_officer";
  static String permission_delivery_agent = "vt_core.vt_app_delivery_boy";
  static String permission_salesman_update_item_rate =
      "vt_core.vt_update_item_rate";
  static String permission_salesman_app_can_use = "vt_core.vt_app_salesman";
}
