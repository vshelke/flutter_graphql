String queryPriceBenchmarkSummary = """
  query priceBenchmarkSummary(\$date:Date,\$marketYardId:ID,\$commodityGroupId:ID,\$first:Int,\$skip:Int,\$query:String){
    priceBenchmarkSummary(filter: {
        date_Gte: \$date,
        date_Lte: \$date
        marketYardId:\$marketYardId
        commodityGroupId: \$commodityGroupId
    },
        first: \$first,
        skip: \$skip,
        query:\$query
    ) {
        date
        commodityGroup{
            id
            name
        }
        marketYard{
            id
            name
        }
        grade {
            id
            name
            specifications
        }
        isRecommended
        logCount
    }
}
""";

String queryAllPriceBenchmarkRecommendations = """
  query allPriceBenchmarkRecommendations(\$date:Date,\$marketYard:ID,\$commodityGroup:ID){
    allPriceBenchmarkRecommendations(filter: {date_Lte: \$date, date_Gte: \$date, marketYard: \$marketYard, commodityGroup: \$commodityGroup}) {
        id
        benchmarkType

        recommendedPrice{
            price
            priceLimit
            priceUnit{
                id
                name
            }
            uomType {
                id
                name
            }
            pricingDoneOn
        }
        recommendedPrice {
            id
        }
        averagePrice{
            id
        }
        benchmarkedAt{
            id
        }
    }
}
""";

String queryAllPriceBenchmarkData = """
  query allPriceBenchmarkData(\$marketYardId:ID,\$commodityGroupId:ID,\$date:Date){
    allPriceBenchmarkData(filter:{marketYardId:\$marketYardId,commodityGroupId:\$commodityGroupId,date_Lte:\$date,date_Gte:\$date}){
        createdAt
        gradePrices {
            remark
            rejection
            price
            priceLimit
            priceUnit{
                name
            }
        }
        metadata {
            seller {
                name
            }
            benchmarkedBy {
                  fullName
            }
        }

    }
}
""";

String allSellers = """
  query allSellers {
  allSellers {
    id
    name
  }

}
""";

String queryMyAllPriceBenchmarkData = """
 query AllPriceBenchmarkData(\$benchmarkDoneById:ID,\$date_Lte:Date,\$date_Gte:Date,\$marketYardId:ID,\$commodityGroupId:ID) {
    allPriceBenchmarkData(filter: {benchmarkDoneById: \$benchmarkDoneById,
        date_Lte: \$date_Lte, date_Gte: \$date_Gte, marketYardId: \$marketYardId,
        commodityGroupId: \$commodityGroupId}) {
          id
        createdAt
        gradePrices {
          id
            remark
            rejection
            price
            priceLimit
            pricingDoneOn
            remark
            rejection
            priceUnit {
              id
                name
            }
            uomType{
             id
             name
            }
            grade{
              id
              name
            }
        }
        metadata {
          id
            commodityGroup {
              id
                name
            }
            marketYard {
              id
                name
            }
            seller {
                name
                category
            }
            benchmarkedBy {
                fullName
            }
            recordedAt{
            id
            }
        }
    }}
""";
