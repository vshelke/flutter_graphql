const myCupons = """
query myCupons(\$orderItems:[OrderItemStockInput],\$totalAmount:Float){
     myCupons(orderItems:\$orderItems,totalAmount:\$totalAmount){
        id
        name
        discountConditions
        percentage
        code
        isActive
    }
}
""";
