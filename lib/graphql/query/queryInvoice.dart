String queryorderInvoiceDetail = """
  query orderInvoiceDetail(\$orderId:ID){
  orderInvoiceDetail(orderId: \$orderId) {
    id
    trackingCode
    pocName
    pocNumber
    invoiceItems {
      name
      quantity
      actualRate
      discountedRate
      category
      itemType
      amount
      balance
    }
    discountCode
    discountPercentage
    deliveryAddress {
      village
      address
    }
    availablePaymentModes
  }
}

""";
