import 'package:fleet_flutter/helper/IConstant.dart';
import "package:flutter/material.dart";
import "package:graphql_flutter/graphql_flutter.dart";
import 'package:shared_preferences/shared_preferences.dart';

class GraphQLConfiguration {
  static var userToken;
  void gettoken() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    userToken = "JWT " + prefs.getString(IConstanct.token);
    print(userToken);
  }

  static HttpLink httpLink =
      HttpLink(uri: "https://staging.vesatogo.com/api/v3/", headers: {
    "Authorization": userToken,
    "Tenant": "alpha",
  });

  ValueNotifier<GraphQLClient> client = ValueNotifier(
    GraphQLClient(
      link: httpLink,
      cache: OptimisticCache(dataIdFromObject: typenameDataIdFromObject),
    ),
  );

  GraphQLClient clientToQuery() {
    gettoken();
    return GraphQLClient(
      cache: OptimisticCache(dataIdFromObject: typenameDataIdFromObject),
      link: httpLink,
    );
  }
}
