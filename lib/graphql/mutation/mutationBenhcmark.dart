String mutationAddRecommendation = """
  mutation AddRecommendationNew(
    \$id: ID
    \$date: Date!
    \$gradeId: ID!
    \$commodityGroupId: ID!
    \$marketYardId: ID!
     \$benchmarkType: String!
    \$benchmarkingDoneInId:ID,
    \$averagePrice: PriceInput!
    \$recommendedPrice: PriceInput!,
    \$benchmarkedAt:AddressInput!,
   
) {
    addBenchmarkRecommendation(
        id: \$id
        date: \$date
        gradeId: \$gradeId
        marketYardId: \$marketYardId
        commodityGroupId: \$commodityGroupId
        recommendedPrice: \$recommendedPrice
        averagePrice: \$averagePrice
        benchmarkType: \$benchmarkType
        benchmarkedAt: \$benchmarkedAt,
        benchmarkingDoneInId:\$benchmarkingDoneInId
    ) {
        banchmarkRecommendation {
            id
            date
            recommendedPrice {
                id
            }
            averagePrice {
                id
            }
        }
    }
}


""";

String mutationAddBenchmark = """
  mutation AddBenchmark(\$metaDataId:ID,\$date:Date!,\$seller:SellerInput,\$marketYardId:ID!,
  \$recordedAt: AddressInput,
  \$commodityGroupId:ID!,
  \$gradeId:ID!,
  \$photos:[MediaInput],
  \$prices:[PriceInput],
 ){
    addBenchmark(
        id: \$metaDataId
        date: \$date
        seller: \$seller
        marketYardId: \$marketYardId
        priceData: { 
          gradeId: \$gradeId, photos: \$photos, prices: \$prices}
        recordedAt: \$recordedAt
        commodityGroupId: \$commodityGroupId

    ) {
        banchmarkMetadata {
            id

            seller {
                id
                name
            }
            recordedAt {
                id
                latitude
                longitude
            }
            pricebenchmarkdataSet {
                id
                grade {
                    id
                    name
                }
                photos {
                    id
                    url
                }
                gradePrices {
                    id
                    isDefault
                    price
                    priceLimit
                }
            }
        }
    }
}


""";
String mutationAddBenchmarkUpdate = """
  mutation AddBenchmark(\$metaDataId:ID,\$benchmarkId:ID,\$date:Date!,\$seller:SellerInput,\$marketYardId:ID!,
  \$recordedAt: AddressInput,
  \$commodityGroupId:ID!,
  \$gradeId:ID!,
  \$photos:[MediaInput],
  \$prices:[PriceInput],
 ){
    addBenchmark(
        id: \$metaDataId
        date: \$date
        seller: \$seller
        marketYardId: \$marketYardId
        priceData: { 
          id:\$benchmarkId,
          gradeId: \$gradeId, photos: \$photos, prices: \$prices}
        recordedAt: \$recordedAt
        commodityGroupId: \$commodityGroupId

    ) {
        banchmarkMetadata {
            id

            seller {
                id
                name
            }
            recordedAt {
                id
                latitude
                longitude
            }
            pricebenchmarkdataSet {
                id
                grade {
                    id
                    name
                }
                photos {
                    id
                    url
                }
                gradePrices {
                    id
                    isDefault
                    price
                    priceLimit
                }
            }
        }
    }
}


""";
