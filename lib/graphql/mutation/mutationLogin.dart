String addStar = """
  mutation AddStar(\$starrableId: ID!) {
    addStar(input: {starrableId: \$starrableId}) {
      starrable {
        viewerHasStarred
      }
    }
  }
""";
String login = """
  mutation Login(\$username:String!,\$password:String!,\$firebaseToken:String){
  login(username: \$username, password: \$password, firebaseToken:\$firebaseToken) {
    token
    me {
      id
      fullName
      currentDepartment {
        id
        name
        photo
      }
    }
  }
}

""";
