
String mutationChangeDepartment = """
  mutation Login(\$department:Int!){
   switchDepartment(department: \$department) {
    user {
      id
      currentDepartment{
         id
        name
        photo
      }
    }
  }
}

""";
